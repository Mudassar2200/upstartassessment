import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostGuard } from './../../@core/guards/post.guard';
import { UserGuard } from './../../@core/guards/user.guard';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
  },
  // Allow acces to /posts if user has 'catalog.read' permissions
  {
    path: 'posts',
    canActivate: [PostGuard],
    loadChildren: () =>
      import('./posts/posts.module').then((m) => m.PostsModule),
  },
  // Allow acces to /users if user has 'user.read' permissions
  {
    path: 'users',
    canActivate: [UserGuard],
    loadChildren: () =>
      import('./users/users.module').then((m) => m.UsersModule),
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
