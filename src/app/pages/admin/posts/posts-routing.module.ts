import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostDetailsPage } from './post-details/post-details.page';
import { PostsPage } from './posts/posts.page';

const routes: Routes = [
  {
    path: '',
    component: PostsPage,
  },
  {
    path: ':postId',
    component: PostDetailsPage
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostRoutingModule {}
