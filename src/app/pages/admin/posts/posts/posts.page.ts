import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IPost } from './../../../../@core/models/post.model';
import { PostsService } from './../../../../services/posts/posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.page.html',
  styleUrls: ['./posts.page.scss'],
})
export class PostsPage implements OnInit {
  posts$: Observable<IPost[]>;

  constructor(private _postsService: PostsService) {
    this.posts$ = this._postsService.loadPosts();
  }

  ngOnInit(): void {}
}
