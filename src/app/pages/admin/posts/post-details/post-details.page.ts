import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, mergeMap, shareReplay, startWith } from 'rxjs/operators';
import { IPost } from './../../../../@core/models/post.model';
import { PostsService } from './../../../../services/posts/posts.service';
import { UsersService } from './../../../../services/users/users.service';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.page.html',
  styleUrls: ['./post-details.page.scss'],
})
export class PostDetailsPage implements OnInit {
  post$!: Observable<IPost | undefined>;

  constructor(
    private _postsService: PostsService,
    private _usersService: UsersService,
    private _route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.post$ = this._postsService
      .loadPostById(this._route.snapshot.params.postId)
      .pipe(
        mergeMap((res: IPost) => {
          if (res?.userId) {
            return this._usersService.getUserById(res.userId).pipe(
              shareReplay(1),
              map((userRes) => {
                res.user = userRes;
                return res;
              }),
            );
          } else {
            return of(res);
          }
        }),
        shareReplay(1),
        startWith(undefined),
      );
  }
}
