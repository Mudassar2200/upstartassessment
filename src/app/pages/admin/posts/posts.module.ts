import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { PostDetailsPage } from './post-details/post-details.page';
import { PostRoutingModule } from './posts-routing.module';
import { PostsPage } from './posts/posts.page';

@NgModule({
  declarations: [
    PostsPage,
    PostDetailsPage
  ],
  imports: [
    CommonModule,
    PostRoutingModule,
    MatButtonModule,
    MatCardModule,
  ]
})
export class PostsModule { }
