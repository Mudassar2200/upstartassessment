import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserDetailsPage } from './user-details/user-details.page';
import { UsersPage } from './users/users.page';

const routes: Routes = [
  {
    path: '',
    component: UsersPage,
  },
  {
    path: ':userId',
    component: UserDetailsPage
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule {}
