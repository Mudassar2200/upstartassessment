import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IUser } from '../../../../@core/models/user.model';
import { UsersService } from '../../../../services/users/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  users$: Observable<IUser[]>;

  constructor(private _usersService: UsersService) {
    this.users$ = this._usersService.getUsers();
  }

  ngOnInit(): void {}
}
