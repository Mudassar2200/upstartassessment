import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, mergeMap, shareReplay, startWith } from 'rxjs/operators';
import { IUser } from '../../../../@core/models/user.model';
import { PostsService } from '../../../../services/posts/posts.service';
import { UsersService } from '../../../../services/users/users.service';
import { IPost } from './../../../../@core/models/post.model';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.page.html',
  styleUrls: ['./user-details.page.scss'],
})
export class UserDetailsPage implements OnInit {
  user$!: Observable<IUser | undefined>;

  constructor(
    private _postsService: PostsService,
    private _usersService: UsersService,
    private _route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.user$ = this._usersService.getUserById(this._route.snapshot.params.userId)
      .pipe(
        mergeMap((res: IUser) => {
          if (res) {
            return this._postsService.loadPosts().pipe(
              shareReplay(1),
              map((postRes:IPost[]) => {
                res.postCount = postRes.filter((obj) => obj.userId === res.id).length;
                return res;
              }),
            );
          } else {
            return of(res);
          }
        }),
        shareReplay(1),
        startWith(undefined),
      );
  }
}
