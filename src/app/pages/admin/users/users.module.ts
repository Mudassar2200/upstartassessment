import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { UserDetailsPage } from './user-details/user-details.page';
import { UserRoutingModule } from './users-routing.module';
import { UsersPage } from './users/users.page';

@NgModule({
  declarations: [
    UsersPage,
    UserDetailsPage
  ],
  imports: [
    CommonModule,
    CommonModule,
    UserRoutingModule,
    MatButtonModule,
    MatCardModule,
    MatListModule,
  ]
})
export class UsersModule { }
