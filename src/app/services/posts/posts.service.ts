import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiResponse } from './../../@core/models/api-response.model';
import { IPost } from './../../@core/models/post.model';
import { ApiService } from './../api.service';

type postData = IPost[] | IPost | any;

@Injectable({
  providedIn: 'root',
})
export class PostsService extends ApiService<postData> {
  private endpoint = '/posts';
  constructor(protected http: HttpClient) {
    super(http);
  }

  loadPosts(): Observable<postData> {
    return this.get(this.endpoint).pipe(
      map((res: ApiResponse<IPost[]>) => {
        //if response have any error we can show relevant toast message if needed
        if(!res.hasErrors()) {
          return res.data;
        } else {
          return [];
        }
      }),
    );
    // Add a request to get posts using `endpoint`
  }

  loadPostById(id:string): Observable<postData> {
    return this.get(this.endpoint + '/' + id).pipe(
      map((res: ApiResponse<IPost>) => {
        //if response have any error we can show relevant toast message if needed
        if(!res.hasErrors()) {
          return res.data;
        } else {
          return null;
        }
      }),
    );
    // Add a request to get posts using `endpoint`
  }
}
