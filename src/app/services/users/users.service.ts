import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IUser } from '@core/models/user.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiResponse } from './../../@core/models/api-response.model';
import { ApiService } from './../api.service';

type userData = IUser[] | IUser | any;
@Injectable({
  providedIn: 'root',
})
export class UsersService extends ApiService<userData> {
  private endpoint = '/users';
  constructor(protected http: HttpClient) {
    super(http);
  }

  getUsers() {
    return this.get(this.endpoint).pipe(
      map((res: ApiResponse<IUser[]>) => {
        //if response have any error we can show relevant toast message if needed
        if(!res.hasErrors()) {
          return res.data;
        } else {
          return [];
        }
      }),
    );
    // Add a request to get users using `endpoint`
  }

  getUserById(id:string | number): Observable<userData> {
    return this.get(this.endpoint + '/' + id).pipe(
      map((res: ApiResponse<IUser>) => {
        //if response have any error we can show relevant toast message if needed
        if(!res.hasErrors()) {
          return res.data;
        } else {
          return null;
        }
      }),
    );
  }
}
