import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserPermissionsI } from '@core/models/user-permissions.model';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { clear as clearStorage, getItem, setItem, StorageItem } from './../../@core/utils/local-storage.utils';
const permissions = require('./permissions.json');

interface ICredentials {
  login: string;
  password: string;
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  private _isLoggedIn$ = new BehaviorSubject<boolean>(!!getItem(StorageItem.User));
  public readonly isLoggedIn$: Observable<boolean> = this._isLoggedIn$.asObservable();
  get isLoggedIn() : boolean {
    return this._isLoggedIn$.getValue();
  }

  get JwtToken(): string {
    return getItem(StorageItem.JwtToken)?.toString() || '';
  }

  private _userPermissions$ = new BehaviorSubject<UserPermissionsI | null>(<UserPermissionsI>getItem(StorageItem.User));
  public readonly userPermissions$: Observable<UserPermissionsI | null> = this._userPermissions$.asObservable();
  get permissions() {
    return this._userPermissions$.getValue();
  }

  constructor(private router: Router) {}


  login(credentials: ICredentials) {
    this.fakeLogin().pipe(
      take(1),
      tap((res) => {
      setItem(StorageItem.User, res);
      //for future use if we have jwtToken
      setItem(StorageItem.JwtToken, res?.permissions?.jwtToken|| null);
      this._isLoggedIn$.next(true);
      this._userPermissions$.next(res);
    })).subscribe((res) => {
      this.router.navigate(['/admin']);
    });
  }

  logout() {
    this._isLoggedIn$.next(false);
    this._userPermissions$.next(null);
    clearStorage();
    this.router.navigate(['/login']);
  }

  private fakeLogin() {
    return of({ permissions });
  }
}
