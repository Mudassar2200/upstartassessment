import { IGeo } from "./geo.model";

export interface IAddress {
  street:  string;
  suite:   string;
  city:    string;
  zipcode: string;
  geo:     IGeo;
}
