export interface Response {
  status: boolean;
  data: any;
}

export class ApiResponse<T> {

  headers: any;
  status!: boolean;
  data!: T;
  errors?: ApiError[] | any;

  constructor() {
    this.errors = [];
  }

  getErrorsText(): string {
    return this.errors.map((e:any) => e.text).join(' ');
  }

  hasErrors(): boolean {
    return this.errors.length > 0;
  }
}

export class ApiError {
  code!: ErrorCode;
  text!: string;
}

export enum ErrorCode {
  UnknownError = 1,
  OrderIsOutdated = 2
}
