import { IUser } from "./user.model";

export interface IPost {
  userId: number;
  id: number;
  title: string;
  body: string;
  user?: IUser;
}
